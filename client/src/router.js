import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Products from './views/Products.vue'
import ProductModal from './components/ProductModal'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/products',
      name: 'products',
      component: Products,
      children: [
        { path: '/product/:id', name: 'productModal', component: ProductModal },
        {
          path: 'search/:phrase?',
          name: 'search',
          children: [
            {
              path: 'page/:page?',
              name: 'searchPage'
            }
          ]
        },
        {
          path: 'page/:page?',
          name: 'page'
        }
      ]
    }
  ]
})
