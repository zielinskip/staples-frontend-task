import axios from 'axios'
const API_URL = "http://localhost:3005";

const state = {
  cartData: [],
  cartProducts: []
};

const getters = {
  cartData: state => JSON.stringify(state.cartData),
  cartLength: state => {
    let length = 0;
    state.cartData.forEach(el => {
      length += el.quantity;
    });
    return length
  },
  cartProducts: state => state.cartProducts
};

const actions = {
  async addToCart({ commit }, { id, quantity }) {
    quantity = parseInt(quantity);
    // Check if item is already in cart
    let cartItem = state.cartData.find(c => c.id === id);

    if(cartItem) {
      cartItem.quantity += quantity;
    } else {
      state.cartData.push({
        id: id,
        quantity: quantity
      });
    }

    commit('saveCart');
  },

  async fetchCartItems({commit}) {
    commit('loadCart');

    let products = [];
    if(state.cartData.length > 0) {
      let request = state.cartData.map(el => el.id);

      const response = await axios.get(API_URL+"/products", {
        params: {
          id: request
        }
      });
  
      products = response.data.map(function(el) {
        return {
          id: el.id,
          name: el.general.name,
          thumb: el.images.primary.large,
          quantity: state.cartData.find(c => c.id === el.id).quantity
        }
      });
    }
    commit('setCartProducts', products);
  },

  async removeFromCart({commit}, id) {
    state.cartData = state.cartData.filter(c => c.id !== id);
    commit('saveCart');
    this.dispatch("fetchCartItems");
  },

  async clearCart({commit}) {
    state.cartData = [];
    commit('saveCart');
    this.dispatch("fetchCartItems");
  }
};

const mutations = {
  saveCart(state) {
    localStorage.setItem('cart', JSON.stringify(state.cartData));
  },
  loadCart(state) {
    if(localStorage.getItem('cart')) state.cartData = JSON.parse(localStorage.getItem('cart'));
  },
  setCartProducts: (state, products) => (state.cartProducts = products)
};

export default {
  state,
  getters,
  actions,
  mutations
};