import axios from 'axios'
const API_URL = "http://localhost:3005";

const state = {
  products: [],
  currentProduct: null,
  allProductsCount: null,

  currentPage: 1,
  allPagesCount: 0,
  limitFilter: 15
};

const getters = {
  products: state => state.products,
  currentProduct: state => state.currentProduct,
  allProductsCount: state => state.allProductsCount,
  currentPage: state => state.currentPage,
  limit: state => state.limitFilter,
  allPagesCount: state => state.allPagesCount
};

const actions = {

  async fetchProducts({ commit }, search) {
    let limit = state.limitFilter;
    const response = await axios.get(API_URL+"/products", {
      params: {
        q: search,
        _limit: limit,
        _page: state.currentPage
      }
    });

    commit('setProductsCount', response.headers["x-total-count"]);
    commit('setPagesCount', Math.ceil(response.headers["x-total-count"]/limit));
    commit('setProducts', response.data);

    if(state.currentPage > state.allPagesCount) {
      commit('setCurrentPage', state.allPagesCount);
      this.dispatch('fetchProducts', search);
    }
  },

  async fetchProduct({ commit }, id) {
    const response = await axios.get(API_URL+`/products/${id}`);
    commit('setCurrentProduct', response.data);
  }
};

const mutations = {
  setProducts: (state, products) => (state.products = products),
  setCurrentProduct: (state, currentProduct) => (state.currentProduct = currentProduct),
  setFilter: (state, limitFilter) => (state.limitFilter = limitFilter),
  setProductsCount: (state, allProductsCount) => (state.allProductsCount = allProductsCount),
  setCurrentPage: (state, currentPage) => (state.currentPage = currentPage),
  setPagesCount: (state, allPagesCount) => (state.allPagesCount = allPagesCount)
};

export default {
  state,
  getters,
  actions,
  mutations
};