import Vuex from 'vuex'
import Vue from 'vue'

import products from './modules/products'
import shoppingcart from './modules/shoppingcart'

Vue.use(Vuex);
export default new Vuex.Store({
  modules: {
    products,
    shoppingcart
  }
});